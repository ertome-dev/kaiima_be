<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiFuncs;
use Intervention\Image\ImageManagerStatic as Image;

class MainController extends Controller
{
    public function datacenter(ApiFuncs $aplicc){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

        $result = array();

        //$_POST['data'] = "{\"tag\":\"create_user\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"user_email\":\"petya@vasya.com\", \"pass\":\"111111\"}";
        //$_POST['data'] = "{\"tag\":\"check_user_token\", \"token\":\"e96618e8f2582cf771ee62a59425fa16\"}";
        //$_POST['data'] = "{\"tag\":\"check_admin_token\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\"}";
        //$_POST['data'] = "{\"tag\":\"check_email\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"email\":\"vasya@vasya.com\"}";
        //$_POST['data'] = "{\"tag\":\"login_admin\", \"email\":\"admin@admin.com\", \"pass\":\"111111\"}";
        //$_POST['data'] = "{\"tag\":\"login_simple_user\", \"email\":\"petya@vasya.com\", \"pass\":\"111111\"}";
        //$_POST['data'] = "{\"tag\":\"get_user_token\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"email\":\"vasya@vasya.com\" }";
        //$_POST['data'] = "{\"tag\":\"update_admin_email\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"email\":\"adm@adm.com\"}";
        //$_POST['data'] = "{\"tag\":\"get_admin_email\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\"}";

        //$_POST['data'] = "{\"tag\":\"get_regions\", \"token\":\"e96618e8f2582cf771ee62a59425fa16\"}";

        //$_POST['data'] = "{\"tag\":\"create_gallerie\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"gallerie\":{\"regionId\":1,\"name\":\"ANNEX 2 - AGRICULTURAL MACHINES\",\"pos_num\":\"1\",\"subGalleries\":[{\"name\":\"Rust (Melampsora ricini)\",\"description\":\"Appears in warm humid conditions on the bottom side of the leaf. Delivered by winds and by humans/ animals when moving from infected  elds into a healthy  eld. Reduces leaf area and photosynthesis ability of the plants.\",\"photos\":[{\"description\":\"test image 1\",\"image\":\"./assets/imgs/gallery_ex/ex_1.png\"},{\"description\":\"test image 2\",\"image\":\"./assets/imgs/gallery_ex/ex_2.png\"},{\"description\":\"test image 3\",\"image\":\"./assets/imgs/gallery_ex/ex_3.png\"},{\"description\":\"test image 4\",\"image\":\"./assets/imgs/gallery_ex/ex_4.png\"}]},{\"name\":\"Cercospora (Cercospora ricini)\",\"description\":\"appears in cold humid conditions. Usually starts on the lower, old leaves and “climbs” upwards. Delivered by winds and by humans/animals when moving from infected  eld into a healthy  eld. Reduces leaf area and photosynthesis ability of the plants. Attacks all parts of the plant in advanced stages.\",\"photos\":[{\"description\":\"test image 2\",\"image\":\"./assets/imgs/gallery_ex/ex_3.png\"},{\"description\":\"test image 1\",\"image\":\"./assets/imgs/gallery_ex/ex_5.png\"},{\"description\":\"test image 4\",\"image\":\"./assets/imgs/gallery_ex/ex_1.png\"},{\"description\":\"test image 3\",\"image\":\"./assets/imgs/gallery_ex/ex_2.png\"}]}]}}";
        //$_POST['data'] = "{\"tag\":\"get_gallrie_by_id\", \"token\":\"e96618e8f2582cf771ee62a59425fa16\", \"id\":\"5\"}";
        //$_POST['data'] = "{\"tag\":\"get_gallrie_by_name\", \"token\":\"e96618e8f2582cf771ee62a59425fa16\", \"reg_id\":\"1\", \"gal_name\":\"ANNEX\"}";
        //$_POST['data'] = "{\"tag\":\"get_galleries\", \"token\":\"e96618e8f2582cf771ee62a59425fa16\", \"reg_id\":\"1\"}";
        //$_POST['data'] = "{\"tag\":\"delete_gallery\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"gal_id\":\"6\"}";
        //$_POST['data'] = "{\"tag\":\"delete_sub_gallery\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"sub_gal_id\":\"6\"}";
        //$_POST['data'] = "{\"tag\":\"add_sub_categorie\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"subCategorie\": {\"gal_id\":5,\"name\":\"1 Rust (Melampsora ricini)!\",\"description\":\"Appears in warm humid conditions on the bottom side of the leaf. Delivered by winds and by humans\/ animals when moving from infected elds into a healthy eld.\",\"photos\":[{\"id\":10,\"description\":\"test image 11\",\"image\":null},{\"id\":11,\"description\":\"test image 2\",\"image\":null},{\"id\":12,\"description\":\"test image 3\",\"image\":null},{\"id\":13,\"description\":\"test image 4\",\"image\":null}]}}";
        //$_POST['data'] = "{\"tag\":\"update_gallerie\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"gallerie\":{\"regionId\":1,\"id\":36,\"pos_num\":\"5\",\"name\":\"ANNEX 22 - AGRICULTURAL MACHINES\",\"subGalleries\":[{\"id\":\"6\",\"name\":\"1 Rust (Melampsora ricini)\",\"description\":\"Appears in warm humid conditions on the bottom side of the leaf. Delivered by winds and by humans/ animals when moving from infected  elds into a healthy  eld.\",\"photos\":[{\"id\":\"10\",\"description\":\"test image 11\",\"image\":\"./assets/imgs/gallery_ex/ex_1.png\"}]}]}}";

        //$_POST['data'] = "{\"tag\":\"create_image_protocol\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"imageProtocols\":[{\"regionId\":1,\"pos_num\":\"1\",\"name\":\"Castor Growth Stages\",\"description\":\"The illustration below details the castor plant’s growth stages as de ned by Kaiima. We references these stages at certain points in the growing protocol. Please note that this illustration is for reference purposes only and provides a general concept of how the castor plant grows in the  eld. Also note that Kaiima developed di erent hybrid varieties that di er in maturation (100-150 days). Consult with a Kaiima agronomist to determine which hybrid is best suited to your growing environment.\",\"stages\":[{\"id\":1,\"name\":\"E\",\"image\":\"./assets/imgs/gallery_ex/ex_1.png\"},{\"id\":2,\"name\":\"V10\",\"image\":\"./assets/imgs/gallery_ex/ex_2.png\"},{\"id\":3,\"name\":\"V20\",\"image\":\"./assets/imgs/gallery_ex/ex_3.png\"},{\"id\":4,\"name\":\"P30\",\"image\":\"./assets/imgs/gallery_ex/ex_4.png\"},{\"id\":5,\"name\":\"P40\",\"image\":\"./assets/imgs/gallery_ex/ex_5.png\"},{\"id\":6,\"name\":\"M60\",\"image\":\"./assets/imgs/gallery_ex/ex_1.png\"},{\"id\":7,\"name\":\"M70\",\"image\":\"./assets/imgs/gallery_ex/ex_2.png\"}]}]}";
        //$_POST['data'] = "{\"tag\":\"update_image_protocol\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"imageProtocols\":[{\"regionId\":1,\"id\":\"28\",\"pos_num\":\"8\",\"name\":\"Castor Growth Stagesss\",\"description\":\"The illustration below details the castor plant’s growth stages as de ned by Kaiima. We references these stages at certain points in the growing protocol. Please note that this illustration is for reference purposes only and provides a general concept of how the castor plant grows in the  eld. Also note that Kaiima developed di erent hybrid varieties that di er in maturation (100-150 days). Consult with a Kaiima agronomist to determine which hybrid is best suited to your growing environment.\",\"stages\":[{\"id\":1,\"name\":\"Ee\",\"image\":\"./assets/imgs/gallery_ex/ex_1.png\"},{\"id\":2,\"name\":\"V10\",\"image\":\"./assets/imgs/gallery_ex/ex_2.png\"},{\"id\":3,\"name\":\"V20\",\"image\":\"./assets/imgs/gallery_ex/ex_3.png\"},{\"id\":4,\"name\":\"P30\",\"image\":\"./assets/imgs/gallery_ex/ex_4.png\"},{\"id\":5,\"name\":\"P40\",\"image\":\"./assets/imgs/gallery_ex/ex_5.png\"},{\"id\":6,\"name\":\"M60\",\"image\":\"./assets/imgs/gallery_ex/ex_1.png\"},{\"id\":7,\"name\":\"M70\",\"image\":\"./assets/imgs/gallery_ex/ex_2.png\"}]}]}";
        //$_POST['data'] = "{\"tag\":\"get_protocol_by_id\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"id\":\"6\" }";
        //$_POST['data'] = "{\"tag\":\"get_image_protocols\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"regionId\":\"1\" }";
        //$_POST['data'] = "{\"tag\":\"search_image_protocols\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"name\":\"Castor\", \"regionId\":\"1\" }";
        //$_POST['data'] = "{\"tag\":\"delete_image_protocol\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"id\":\"2\" }";
        //$_POST['data'] = "{\"tag\":\"add_stage\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"im_pr_id\":\"6\", \"stage\":{\"id\":18,\"name\":\"q1\",\"image\":\"\"}}";
        //$_POST['data'] = "{\"tag\":\"delete_stage\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"stg_id\":\"25\" }";

        //$_POST['data'] = "{\"tag\":\"create_text_protocol\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"textProtocols\":[{\"regionId\":3,\"pos_num\":\"1\",\"name\":\"Weed Control\",\"descriptionGroup\":{\"name\":\"Herbicides\",\"value\":\"Below are the recommended chemicals by stage. Always apply agrochemicals according to the manufacturer’s instructions.\"},\"selectsGroup\":{\"name\":\"Select Growing Stage\",\"values\":[{\"id\":1,\"name\":\"Pre-sowing\",\"description\":\"\",\"subSelectGroup\":{\"name\":\"Select By Commercial name is israel\",\"values\":[{\"id\":1,\"name\":\"TITUS\",\"values\":[{\"name\":\"Rate (dose)\",\"value\":\"60 gr/Ha\"},{\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]},{\"id\":2,\"name\":\"SANDEA / PERMIT\",\"values\":[{\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"name\":\"Formulation\",\"value\":\"EC\"},{\"name\":\"Concentration (of active ingredient)\",\"value\":\"75%\"},{\"name\":\"Manufacturer\",\"value\":\"Gowan\"},{\"name\":\"Rate (dose)\",\"value\":\"60 gr/Ha\"},{\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]},{\"id\":3,\"name\":\"CLASSIC / KOLBEN\",\"values\":[{\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"name\":\"Formulation\",\"value\":\"EC\"}]},{\"id\":4,\"name\":\"FRONTIER / OPTIMA\",\"values\":[{\"name\":\"Rate (dose)\",\"value\":\"60 gr/Ha\"}]}]}},{\"id\":2,\"name\":\"Pre-emergence\",\"description\":\"\",\"subSelectGroup\":{\"name\":\"Simple text\",\"values\":[{\"id\":1,\"name\":\"SANDEA / PERMIT\",\"values\":[{\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"}]}]}},{\"id\":3,\"name\":\"Post-emergence\",\"description\":\"against broad-leaves (selective)\",\"subSelectGroup\":{\"name\":\"Select By Commercial name is israel\",\"values\":[{\"id\":1,\"name\":\"SANDEA / PERMIT\",\"values\":[{\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"name\":\"Formulation\",\"value\":\"EC\"},{\"name\":\"Concentration (of active ingredient)\",\"value\":\"75%\"},{\"name\":\"Manufacturer\",\"value\":\"Gowan\"},{\"name\":\"Rate (dose)\",\"value\":\"60 gr/Ha\"},{\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]}]}},{\"id\":4,\"name\":\"Post-emergence\",\"description\":\"against broad-leaves (non-selective)\",\"subSelectGroup\":{\"name\":\"Select By Commercial name is israel\",\"values\":[{\"id\":1,\"name\":\"SANDEA / PERMIT\",\"values\":[{\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"name\":\"Formulation\",\"value\":\"EC\"},{\"name\":\"Concentration (of active ingredient)\",\"value\":\"75%\"},{\"name\":\"Manufacturer\",\"value\":\"Gowan\"},{\"name\":\"Rate (dose)\",\"value\":\"60 gr/Ha\"},{\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]}]}},{\"id\":5,\"name\":\"Post-emergence\",\"description\":\"against grasses\",\"subSelectGroup\":{\"name\":\"Select By Commercial name is israel\",\"values\":[{\"id\":1,\"name\":\"SANDEA / PERMIT\",\"values\":[{\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"name\":\"Formulation\",\"value\":\"EC\"},{\"name\":\"Concentration (of active ingredient)\",\"value\":\"75%\"},{\"name\":\"Manufacturer\",\"value\":\"Gowan\"},{\"name\":\"Rate (dose)\",\"value\":\"60 gr/Ha\"},{\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]}]}}]}},{\"regionId\":1,\"pos_num\":\"2\",\"name\":\"Test Protocol\",\"descriptionGroup\":{\"name\":\"whatever\",\"value\":\"Lorem ipsum dolor sit amet, has dolorum sententiae an. Sed eu nemore alienum efficiendi. Dicant legere ne vim, vim in tantas blandit democritum, nec choro ignota qualisque ne. Omnes oporteat hendrerit te mea. Eleifend dissentias per ne, et erant nostro honestatis sit.\"},\"selectsGroup\":{\"name\":\"Select what you want\",\"values\":[{\"id\":1,\"name\":\"Pre-sowing\",\"description\":\"lksdfj asl;dfk al;k  as;dk a;ldk as;ldk as asdkal;sa;sl das;ldk ;alskd\",\"subSelectGroup\":{\"name\":\"Select By Commercial name is brasil (for example)\",\"values\":[{\"id\":1,\"name\":\"SANDEA / PERMIT\",\"values\":[{\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"name\":\"Formulation\",\"value\":\"EC\"},{\"name\":\"Concentration (of active ingredient)\",\"value\":\"75%\"},{\"name\":\"Manufacturer\",\"value\":\"Gowan\"},{\"name\":\"Rate (dose)\",\"value\":\"60 gr/Ha\"},{\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]}]}}]}}]}";
        //$_POST['data'] = "{\"tag\":\"get_text_protocol_by_id\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"id\":\"6\" }";
        //$_POST['data'] = "{\"tag\":\"get_text_protocol_by_region\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"regionId\":\"1\" }";
        //$_POST['data'] = "{\"tag\":\"update_text_protocol\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"textProtocols\":{\"id\":93,\"pos_num\":\"9\",\"regionId\":3,\"name\":\"Weed Control\",\"descriptionGroup\":{\"id\":6,\"prt_id\":6,\"name\":\"Herbicides\",\"value\":\"Below are the recommended chemicals by stage. Always apply agrochemicals according to the manufacturer\u2019s instructions.\"},\"selectsGroup\":{\"id\":6,\"prt_id\":6,\"name\":\"Select Growing Stage\",\"values\":[{\"id\":14,\"sg_id\":6,\"name\":\"Pre-sowing\",\"description\":\"\",\"subSelectGroup\":{\"id\":13,\"sgv_id\":14,\"name\":\"Select By Commercial name is israel\",\"values\":[{\"id\":19,\"ssg_id\":13,\"name\":\"TITUS\",\"values\":[{\"id\":73,\"ssgv_id\":19,\"name\":\"Rate (dose)\",\"value\":\"60 gr\/Ha\"},{\"id\":74,\"ssgv_id\":19,\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]},{\"id\":20,\"ssg_id\":13,\"name\":\"SANDEA \/ PERMIT\",\"values\":[{\"id\":75,\"ssgv_id\":20,\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"id\":76,\"ssgv_id\":20,\"name\":\"Formulation\",\"value\":\"EC\"},{\"id\":77,\"ssgv_id\":20,\"name\":\"Concentration (of active ingredient)\",\"value\":\"75%\"},{\"id\":78,\"ssgv_id\":20,\"name\":\"Manufacturer\",\"value\":\"Gowan\"},{\"id\":79,\"ssgv_id\":20,\"name\":\"Rate (dose)\",\"value\":\"60 gr\/Ha\"},{\"id\":80,\"ssgv_id\":20,\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]},{\"id\":21,\"ssg_id\":13,\"name\":\"CLASSIC \/ KOLBEN\",\"values\":[{\"id\":81,\"ssgv_id\":21,\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"id\":82,\"ssgv_id\":21,\"name\":\"Formulation\",\"value\":\"EC\"}]},{\"id\":22,\"ssg_id\":13,\"name\":\"FRONTIER \/ OPTIMA\",\"values\":[{\"id\":83,\"ssgv_id\":22,\"name\":\"Rate (dose)\",\"value\":\"60 gr\/Ha\"}]}]}},{\"id\":15,\"sg_id\":6,\"name\":\"Pre-emergence\",\"description\":\"\",\"subSelectGroup\":{\"id\":14,\"sgv_id\":15,\"name\":\"Simple text\",\"values\":[{\"id\":23,\"ssg_id\":14,\"name\":\"SANDEA \/ PERMIT\",\"values\":[{\"id\":84,\"ssgv_id\":23,\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"}]}]}},{\"id\":16,\"sg_id\":6,\"name\":\"Post-emergence\",\"description\":\"against broad-leaves (selective)\",\"subSelectGroup\":{\"id\":15,\"sgv_id\":16,\"name\":\"Select By Commercial name is israel\",\"values\":[{\"id\":24,\"ssg_id\":15,\"name\":\"SANDEA \/ PERMIT\",\"values\":[{\"id\":85,\"ssgv_id\":24,\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"id\":86,\"ssgv_id\":24,\"name\":\"Formulation\",\"value\":\"EC\"},{\"id\":87,\"ssgv_id\":24,\"name\":\"Concentration (of active ingredient)\",\"value\":\"75%\"},{\"id\":88,\"ssgv_id\":24,\"name\":\"Manufacturer\",\"value\":\"Gowan\"},{\"id\":89,\"ssgv_id\":24,\"name\":\"Rate (dose)\",\"value\":\"60 gr\/Ha\"},{\"id\":90,\"ssgv_id\":24,\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]}]}},{\"id\":17,\"sg_id\":6,\"name\":\"Post-emergence\",\"description\":\"against broad-leaves (non-selective)\",\"subSelectGroup\":{\"id\":16,\"sgv_id\":17,\"name\":\"Select By Commercial name is israel\",\"values\":[{\"id\":25,\"ssg_id\":16,\"name\":\"SANDEA \/ PERMIT\",\"values\":[{\"id\":91,\"ssgv_id\":25,\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"id\":92,\"ssgv_id\":25,\"name\":\"Formulation\",\"value\":\"EC\"},{\"id\":93,\"ssgv_id\":25,\"name\":\"Concentration (of active ingredient)\",\"value\":\"75%\"},{\"id\":94,\"ssgv_id\":25,\"name\":\"Manufacturer\",\"value\":\"Gowan\"},{\"id\":95,\"ssgv_id\":25,\"name\":\"Rate (dose)\",\"value\":\"60 gr\/Ha\"},{\"id\":96,\"ssgv_id\":25,\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]}]}},{\"id\":18,\"sg_id\":6,\"name\":\"Post-emergence\",\"description\":\"against grasses\",\"subSelectGroup\":{\"id\":17,\"sgv_id\":18,\"name\":\"Select By Commercial name is israel\",\"values\":[{\"id\":26,\"ssg_id\":17,\"name\":\"SANDEA \/ PERMIT\",\"values\":[{\"id\":97,\"ssgv_id\":26,\"name\":\"Active ingredient\",\"value\":\"HALOSULFURON METHYL\"},{\"id\":98,\"ssgv_id\":26,\"name\":\"Formulation\",\"value\":\"EC\"},{\"id\":99,\"ssgv_id\":26,\"name\":\"Concentration (of active ingredient)\",\"value\":\"75%\"},{\"id\":100,\"ssgv_id\":26,\"name\":\"Manufacturer\",\"value\":\"Gowan\"},{\"id\":101,\"ssgv_id\":26,\"name\":\"Rate (dose)\",\"value\":\"60 gr\/Ha\"},{\"id\":102,\"ssgv_id\":26,\"name\":\"Comments\",\"value\":\"Can be sprayed on plants when they have at least 4 leaves\"}]}]}}]}}} ";
        //$_POST['data'] = "{\"tag\":\"search_text_protocol\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"name\":\"Weed\", \"regionId\":\"3\" }";
        //$_POST['data'] = "{\"tag\":\"delete_text_protocol\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"id\":\"7\"}";
        //$_POST['data'] = "{\"tag\":\"delete_SelectsGroup_Values_SubSelectGroup_values_values\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"id\":\"108\"}";
        //$_POST['data'] = "{\"tag\":\"delete_SelectsGroup_Values_SubSelectGroup_values\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"id\":\"27\"}";
        //$_POST['data'] = "{\"tag\":\"add_SelectsGroup_Values_SubSelectGroup_values_values\", \"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"values\":{\"ssgv_id\":22,\"name\":\"Rate (dose)\",\"value\":\"60 gr\/Ha\"}}";

        //$_POST['data'] = "{\"tag\":\"create_categorie\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"categories\":[{\"regionId\":1,\"pos_num\":\"1\",\"id\":1,\"name\":\"About Kaiima\",\"subCategories\":[{\"id\":1,\"name\":\"Company Description\",\"description\":\"Kaiima Bio-Agritech is a genetics and breeding technology company that developed a proprietary, non-GMO platform called EPTM. EPTM technology unlocks a new paradigm in crop productivity by inducing novel elite diversity within the genome, using the plant’s own DNA. The technology creates superior performing genetics in breeding programs and works with all major crops and plant species. EPTM boosts the inherent productivity and resource usage e ciency of crops - creating value for companies who develop improved seed varieties and to farmers who grow them.\"},{\"id\":2,\"name\":\"Castor Maxx\",\"description\":\"CastorMaxx© is an integrated castor cultivation system pioneered by Kaiima, that combines elite hybrid seed with advanced, large-scale growing protocols, enabling growers to scale- up their castor farming and achieve breakthrough bean and oil yield potential. The CastorMaxx© system optimizes production through customized and scalable solutions – sustainably and economically producing castor oil as a renewable feedstock for the high-value, bio-based products industry. Kaiima is headquartered in Moshav Sharona, located in the Lower Galilee, Israel, and also operates regionally through subsidiaries in the U.S. and China. More information about the company and its activities can be found at www.kaiima.com\"},{\"id\":3,\"name\":\"Sub Category Title\",\"description\":\"Mei ad elit idque liber, pro nostrud nominavi vulputate ex. Sed ne dolore nominati, ex usu summo consetetur definitionem. Mea eruditi mentitum argumentum in, cum quod lobortis ex, in mel inani delicata urbanitas.\"}]},{\"regionId\":1,\"pos_num\":\"2\",\"id\":2,\"name\":\"Usu tollit\",\"subCategories\":[{\"id\":1,\"name\":\"Sed ne dolore nominati\",\"description\":\"Sed ne dolore nominati, ex usu summo consetetur definitionem. Mea eruditi mentitum argumentum in, cum quod lobortis ex, in mel inani delicata urbanitas. Mel an simul dolorum maluisset, esse erant definitionem ea eum.\"},{\"id\":2,\"name\":\"Vim cu stet utinam partem\",\"description\":\"At eam solet discere admodum, quo id elit latine ponderum, dicat impedit nominavi id per. Sea ei corpora philosophia. Ut usu summo legere apeirian, et harum volumus dignissim sea. Ius an utamur aperiri, ea pri sumo nusquam tacimates. Nec ut rebum graecis.\"},{\"id\":3,\"name\":\"Quis nominati ad pri\",\"description\":\"An cum denique postulant mnesarchum, eam quod adhuc appareat in. Mel no labitur vivendum legendos, ut pro elitr assueverit, est fugit vitae appareat cu. Vocent laboramus est at. Id possit luptatum mnesarchum vis. Molestiae voluptaria no vel.\"}]},{\"regionId\":2,\"pos_num\":\"3\",\"id\":3,\"name\":\"Usu tollit\",\"subCategories\":[{\"id\":1,\"name\":\"Sed ne dolore nominati\",\"description\":\"Sed ne dolore nominati, ex usu summo consetetur definitionem. Mea eruditi mentitum argumentum in, cum quod lobortis ex, in mel inani delicata urbanitas. Mel an simul dolorum maluisset, esse erant definitionem ea eum.\"},{\"id\":2,\"name\":\"Vim cu stet utinam partem\",\"description\":\"At eam solet discere admodum, quo id elit latine ponderum, dicat impedit nominavi id per. Sea ei corpora philosophia. Ut usu summo legere apeirian, et harum volumus dignissim sea. Ius an utamur aperiri, ea pri sumo nusquam tacimates. Nec ut rebum graecis.\"},{\"id\":3,\"name\":\"Quis nominati ad pri\",\"description\":\"An cum denique postulant mnesarchum, eam quod adhuc appareat in. Mel no labitur vivendum legendos, ut pro elitr assueverit, est fugit vitae appareat cu. Vocent laboramus est at. Id possit luptatum mnesarchum vis. Molestiae voluptaria no vel.\"}]},{\"regionId\":3,\"pos_num\":\"4\",\"id\":4,\"name\":\"Usu tollit\",\"subCategories\":[{\"id\":2,\"name\":\"Vim cu stet utinam partem\",\"description\":\"At eam solet discere admodum, quo id elit latine ponderum, dicat impedit nominavi id per. Sea ei corpora philosophia. Ut usu summo legere apeirian, et harum volumus dignissim sea. Ius an utamur aperiri, ea pri sumo nusquam tacimates. Nec ut rebum graecis.\"},{\"id\":3,\"name\":\"Quis nominati ad pri\",\"description\":\"An cum denique postulant mnesarchum, eam quod adhuc appareat in. Mel no labitur vivendum legendos, ut pro elitr assueverit, est fugit vitae appareat cu. Vocent laboramus est at. Id possit luptatum mnesarchum vis. Molestiae voluptaria no vel.\"}]}]}";
        //$_POST['data'] = "{\"tag\":\"update_сategorie\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"categories\":{\"id\":15,\"regionId\":2,\"pos_num\":\"8\",\"name\":\"About Kaiima\",\"subCategories\":[{\"id\":1,\"cat_id\":1,\"name\":\"Company Description\",\"description\":\"Kaiima Bio-Agritech is a genetics and breeding technology company that developed a proprietary, non-GMO platform called EPTM. EPTM technology unlocks a new paradigm in crop productivity by inducing novel elite diversity within the genome, using the plant\u2019s own DNA. The technology creates superior performing genetics in breeding programs and works with all major crops and plant species. EPTM boosts the inherent productivity and resource usage e ciency of crops - creating value for companies who develop improved seed varieties and to farmers who grow them.\"},{\"id\":2,\"cat_id\":1,\"name\":\"Castor Maxx\",\"description\":\"CastorMaxx\u00a9 is an integrated castor cultivation system pioneered by Kaiima, that combines elite hybrid seed with advanced, large-scale growing protocols, enabling growers to scale- up their castor farming and achieve breakthrough bean and oil yield potential. The CastorMaxx\u00a9 system optimizes production through customized and scalable solutions \u2013 sustainably and economically producing castor oil as a renewable feedstock for the high-value, bio-based products industry. Kaiima is headquartered in Moshav Sharona, located in the Lower Galilee, Israel, and also operates regionally through subsidiaries in the U.S. and China. More information about the company and its activities can be found at www.kaiima.com\"},{\"id\":3,\"cat_id\":1,\"name\":\"Sub Category Title\",\"description\":\"Mei ad elit idque liber, pro nostrud nominavi vulputate ex. Sed ne dolore nominati, ex usu summo consetetur definitionem.\"}]}}";
        //$_POST['data'] = "{\"tag\":\"search_categorie\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"regionId\":\"2\",\"name\":\"About\"}";
        //$_POST['data'] = "{\"tag\":\"get_categorie_by_region_id\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"regionId\":\"2\"}";
        //$_POST['data'] = "{\"tag\":\"get_categorie_by_id\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"id\":\"2\"}";
        //$_POST['data'] = "{\"tag\":\"delete_categorie\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"id\":\"1\"}";
        //$_POST['data'] = "{\"tag\":\"delete_sub_categorie_categorie\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\",\"id\":\"12\"}";
        //$_POST['data'] = "{\"tag\":\"add_sub_categorie_categorie\",\"token\":\"ec265dbb99886bdbad198b8e45961ad0\", \"subCategories\":{\"cat_id\":2,\"name\":\"Sed ne dolore nominati\",\"description\":\"Sed ne dolore nominati, ex usu summo consetetur definitionem. Mea eruditi mentitum argumentum in, cum quod lobortis ex, in mel inani delicata urbanitas. Mel an simul dolorum maluisset, esse erant definitionem ea eum.\"}}";

        //$_POST['data'] = "";
        $result['success'] = 0;
        $result['message'] = "No input data";
        $result['data'] = 0;
        //dd($_POST);

        if(isset($_POST['data']) && !empty($_POST['data'])){
            $_POST['_token'] = csrf_token();
            $info = json_decode($_POST['data'], true);

            if($info['tag'] == "create_user"){
                $result = $aplicc->addUser($info['token'], $info['user_email'], $info['pass']);
            }
            else if($info['tag'] == "login_simple_user"){
                $result = $aplicc->loginSimpleUser($info['email'], $info['pass']);
            }
            else if($info['tag'] == "check_user_token"){
                $result = $aplicc->isUserTokenLive($info['token']);
            }
            else if($info['tag'] == "check_admin_token"){
                $result = $aplicc->isAdminTokenLive($info['token']);
            }
            else if($info['tag'] == "check_email"){
                $result = $aplicc->isEmailExist($info['token'], $info['email']);
            }
            else if($info['tag'] == "login_admin"){
                $result = $aplicc->loginAdmin($info['email'], $info['pass']);
            }
            else if($info['tag'] == "get_user_token"){
                $result = $aplicc->getUserToken($info['token'], $info['email']);
            }
            else if($info['tag'] == "update_admin_email"){
                $result = $aplicc->updateAdminEmail($info['token'], $info['email']);
            }
            else if($info['tag'] == "get_admin_email"){
                $result = $aplicc->getAdminEmail($info['token']);
            }
            else if($info['tag'] == "get_regions"){
                $result = $aplicc->getRegions($info['token']);
            }
            else if($info['tag'] == "create_gallerie"){
                $result = $aplicc->createGallery($info['token'], $info['gallerie']);
            }
            else if($info['tag'] == "add_sub_categorie"){
                $result = $aplicc->addSubCategorie($info['token'], $info['subCategorie']);
            }
            else if($info['tag'] == "create_gallerie"){
                $result = $aplicc->createGallery($info['token'], $info['gallerie']);
            }
            else if($info['tag'] == "get_gallrie_by_id"){
                $result = $aplicc->getGalleryById($info['token'], $info['id']);
            }
            else if($info['tag'] == "get_gallrie_by_name"){
                $result = $aplicc->getGalleryByName($info['token'], $info['reg_id'], $info['gal_name']);
            }
            else if($info['tag'] == "get_galleries"){
                $result = $aplicc->getGallerys($info['token'], $info['reg_id']);
            }
            else if($info['tag'] == "delete_gallery"){
                $result = $aplicc->deleteGallery($info['token'], $info['gal_id']);
            }
            else if($info['tag'] == "delete_sub_gallery"){
                $result = $aplicc->deleteSubGallery($info['token'], $info['sub_gal_id']);
            }
            else if($info['tag'] == "update_gallerie"){
                $result = $aplicc->updateGallery($info['token'], $info['gallerie']);
            }
            else if($info['tag'] == "create_image_protocol"){
                $result = $aplicc->createImageProtocol($info['token'], $info['imageProtocols']);
            }
            else if($info['tag'] == "add_stage"){
                $result = $aplicc->addStageInImageProtocl($info['token'], $info['im_pr_id'], $info['stage']);
            }
            else if($info['tag'] == "delete_stage"){
                $result = $aplicc->deleteStageInImageProtocl($info['token'], $info['stg_id']);
            }
            else if($info['tag'] == "update_image_protocol"){
                $result = $aplicc->updateImageProtocol($info['token'], $info['imageProtocols']);
            }
            else if($info['tag'] == "get_protocol_by_id"){
                $result = $aplicc->getProtocolById($info['token'], $info['id']);
            }
            else if($info['tag'] == "get_image_protocols"){
                $result = $aplicc->getImageProtocols($info['token'], $info['regionId']);
            }
            else if($info['tag'] == "search_image_protocols"){
                $result = $aplicc->searchImageProtocols($info['token'], $info['name'], $info['regionId']);
            }
            else if($info['tag'] == "delete_image_protocol"){
                $result = $aplicc->deleteImageProtocol($info['token'], $info['id']);
            }
            else if($info['tag'] == "create_text_protocol"){
                $result = $aplicc->createTextProtocol($info['token'], $info['textProtocols']);
            }
            else if($info['tag'] == "get_text_protocol_by_id"){
                $result = $aplicc->getTextProtocolById($info['token'], $info['id']);
            }
            else if($info['tag'] == "get_text_protocol_by_region"){
                $result = $aplicc->getTextProtocolByRegion($info['token'], $info['regionId']);
            }
            else if($info['tag'] == "update_text_protocol"){
                $result = $aplicc->updateTextProtocol($info['token'], $info['textProtocols']);
            }
            else if($info['tag'] == "search_text_protocol"){
                $result = $aplicc->searchTextProtocol($info['token'], $info['name'], $info['regionId']);
            }
            else if($info['tag'] == "delete_text_protocol"){
                $result = $aplicc->deleteTextProtocol($info['token'], $info['id']);
            }
            else if($info['tag'] == "delete_SelectsGroup_Values_SubSelectGroup_values_values"){
                $result = $aplicc->delete_SelectsGroup_Values_SubSelectGroup_values_values($info['token'], $info['id']);
            }
            else if($info['tag'] == "delete_SelectsGroup_Values_SubSelectGroup_values"){
                $result = $aplicc->delete_SelectsGroup_Values_SubSelectGroup_values($info['token'], $info['id']);
            }
            else if($info['tag'] == "add_SelectsGroup_Values_SubSelectGroup_values_values"){
                $result = $aplicc->add_SelectsGroup_Values_SubSelectGroup_values_values($info['token'], $info['values']);
            }
            else if($info['tag'] == "add_SelectsGroup_Values_SubSelectGroup_values"){
                $result = $aplicc->add_SelectsGroup_Values_SubSelectGroup_values($info['token'], $info['values']);
            }
            else if($info['tag'] == "create_categorie"){
                $result = $aplicc->createCategorie($info['token'], $info['categories']);
            }
            else if($info['tag'] == "add_sub_categorie_categorie"){
                $result = $aplicc->addSubCategorieCategorie($info['token'], $info['subCategories']);
            }
            else if($info['tag'] == "get_categorie_by_id"){
                $result = $aplicc->getCategorieById($info['token'], $info['id']);
            }
            else if($info['tag'] == "get_categorie_by_region_id"){
                $result = $aplicc->getCategorieByRegionId($info['token'], $info['regionId']);
            }
            else if($info['tag'] == "update_сategorie"){
                $result = $aplicc->updateCategorie($info['token'], $info['categories']);
            }
            else if($info['tag'] == "search_categorie"){
                $result = $aplicc->searchCategorie($info['token'], $info['regionId'], $info['name']);
            }
            else if($info['tag'] == "delete_categorie"){
                $result = $aplicc->deleteCategorie($info['token'], $info['id']);
            }
            else if($info['tag'] == "delete_sub_categorie_categorie"){
                $result = $aplicc->deleteSubCategorieCategorie($info['token'], $info['id']);
            }
        }

        echo json_encode($result);
    }

    public function addImageAjax(ApiFuncs $apiFuncs){
        $result = 0;
        if(isset($_POST['id']) && isset($_FILES['file'])) {
            $img_id = $_POST['id'];
            $image = $_FILES['file'];

                if (strpos($image['type'], "jpg") || strpos($image['type'], "jpeg") || strpos($image['type'], "png") || strpos($image['type'], "gif")) {
                    $img_name = md5(microtime() . rand(1, 1000000)) . ".jpg";

                    $res_up = $apiFuncs->updateImgDescr($img_id, $img_name);

                    $new_img = Image::make($image['tmp_name']);
                    $new_img->save($_SERVER['DOCUMENT_ROOT'] . "/kiimia/public/images/" . $img_name);
                    $result = 1;
                }
        }

        return $result;
    }
}
