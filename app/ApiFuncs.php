<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ApiFuncs extends Model
{
    //users block
    public function isAdminTokenLive($token){
        $info = DB::table('user_admin')->where('_token', $token)->select('id')->first();

        if($info == null) {
            $result['message'] = "Error. Bad token";
            $result['success'] = 0;
        }
        else{
            $result['message'] = "Success. Good token";
            $result['success'] = 1;
        }

        return $result;
    }

    public function isUserTokenLive($token){
        $info = DB::table('users')->where('_token', $token)->select('id')->first();

        if($info == null) {
            $result['message'] = "Error. Bad token";
            $result['success'] = 0;
        }
        else{
            $result['message'] = "Success. Good token";
            $result['success'] = 1;
        }

        return $result;
    }

    public function loginAdmin($email, $pass){
        $info = DB::table('user_admin')->where('email', $email)->where('pass', md5($pass))->select('_token')->first();

        if ($info != null && $info != false && !empty($info)) {
            $result['message'] = "Success.";
            $result['success'] = 1;
            $result['token'] = $info->_token;
        } else {
            $result['message'] = "Error.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function isEmailExist($token, $email){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $info = DB::table('users')->where('email', $email)->select('_token')->first();

            if ($info == null) {
                $result['message'] = "Success.";
                $result['success'] = 1;
            }
            else {
                $result['message'] = "Error. Such email is already exist";
                $result['success'] = 0;
                $result['token'] = $info->_token;
            }
        }
        else {
            $result['message'] = "Error. Bad token";
            $result['success'] = 0;
            $result['token'] = 0;
        }

        return $result;
    }

    public function addUser($token, $user_email, $pass){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']){
            $isEmailExist = $this->isEmailExist($token, $user_email);

            if($isEmailExist['success'] == 1) {
                $user_token = md5(microtime());
                $info = DB::table('users')->insertGetId(
                    array(
                        'name' => "",
                        'email' => $user_email,
                        'pass' => md5($pass),
                        '_token' => $user_token
                    )
                );

                if ($info != null && $info != false && !empty($info)) {
                    $result['message'] = "Success. There user was created.";
                    $result['success'] = 1;
                    $result['token'] = $user_token;
                } else {
                    $result['message'] = "Error.";
                    $result['success'] = 0;
                    $result['data'] = 0;
                }
            }
            else{
                $result['message'] = "Such email is already exist";
                $result['success'] = 1;
                $result['token'] = $isEmailExist['token'];
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    public function getUserToken($token, $email){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $info = DB::table('users')->where('email', $email)->select('_token')->first();

            if ($info == null) {
                $result['message'] = "Error. No such email";
                $result['success'] = 0;
                $result['token'] = 0;
            } else {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['token'] = $info->_token;
            }
        }
        else {
            $result['message'] = "Bad admin token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    public function loginSimpleUser($email, $pass){
        $info = DB::table('users')->where('email', $email)->where('pass', md5($pass))->select('_token')->first();

        if ($info != null && $info != false && !empty($info)) {
            $result['message'] = "Success.";
            $result['success'] = 1;
            $result['token'] = $info->_token;
        } else {
            $result['message'] = "Error.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function updateAdminEmail($token, $email){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $info = DB::table('admin_email')->where('id', 1)->update(
                array(
                    'email' => $email
                )
            );

            if ($info != null && $info != false && !empty($info)) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    public function getAdminEmail($token){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);

        if($token_flag['success'] || $token_flag_admin['success']) {
            $info = DB::table('admin_email')->where('id', 1)->get();

            if ($info != null && $info != false && !empty($info)) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    //region block
    public function getRegions($token){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);

        if($token_flag['success'] || $token_flag_admin['success']) {
            $info = DB::table('regions')->get();

            if ($info != null && $info != false && !empty($info)) {
                $result['message'] = "Success. There are info.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else if ($info == null) {
                $result['message'] = "No such region.";
                $result['success'] = 0;
                $result['data'] = $info;
            } else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    //gallery block
    public function createGallery($token, $gallery){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            if(!isset($gallery['pos_num']))
                $gallery['pos_num'] = 0;

            $gal_id = $this->addGallery($gallery['regionId'], $gallery['name'], $gallery['pos_num']);
            $sub_gals = $gallery['subGalleries'];
            $sub_gal_ids = array();

            for($i=0; $i<count($sub_gals); $i++){
                $sub_gal_ids[$i] = $this->addSubGallery($gal_id, $sub_gals[$i]);

                $photos = $sub_gals[$i]['photos'];
                $photos_ids = array();
                for($j=0; $j<count($photos); $j++){
                    $photos_ids[$j] = $this->addPhotoDescr($photos[$j]['description']);
                    $res_bind = $this->bindPhotoGallery($gal_id, $sub_gal_ids[$i], $photos_ids[$j]);
                }
            }

            if ($gal_id) {
                $result['message'] = "Success. Gallerie added.";
                $result['success'] = 1;
                $result['gallerie_id'] = $gal_id;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function updateGallery($token, $gallery){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            if(!isset($gallery['pos_num']))
                $gallery['pos_num'] = 0;

            $gal_update = DB::table('galleries')->where('id', $gallery['id'])->update(
                array(
                    'pos_num' => $gallery['pos_num'],
                    'regionId' => $gallery['regionId'],
                    'name' => $gallery['name']
                )
            );

            $sub_gals = $gallery['subGalleries'];
            $sub_gal_ids = array();

            for($i=0; $i<count($sub_gals); $i++){
                $sub_gal_ids[$i] = DB::table('sub_gals')->where('id', $sub_gals[$i]['id'])->update(
                    array(
                        'name' => $sub_gals[$i]['name'],
                        'description' => $sub_gals[$i]['description']
                    )
                );

                if(isset($sub_gals[$i]['photos'])) {
                    $photos = $sub_gals[$i]['photos'];
                    $photos_ids = array();
                    for ($j = 0; $j < count($photos); $j++) {
                        $photos_ids[$j] = DB::table('photos')->where('id', $photos[$j]['id'])->update(
                            array(
                                'description' => $photos[$j]['description']
                            )
                        );
                    }
                }
            }

            if (1) {
                $result['message'] = "Success. Gallerie updated.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function addGallery($reg_id, $name, $pos_num){
        $result = DB::table('galleries')->insertGetId(
            array(
                'regionId' => $reg_id,
                'pos_num' => $pos_num,
                'name' => $name
            )
        );

        return $result;
    }

    public function addSubGallery($gal_id, $sub_gal){
        $result = DB::table('sub_gals')->insertGetId(
            array(
                'gal_id' => $gal_id,
                'name' => $sub_gal['name'],
                'description' => $sub_gal['description']
            )
        );

        return $result;
    }

    public function addPhotoDescr($descr){
        $result = DB::table('photos')->insertGetId(
            array(
                'description' => $descr
            )
        );

        return $result;
    }

    public function bindPhotoGallery($gal_id, $sub_gal_id, $photo_id){
        $result = DB::table('photo_gallerie')->insertGetId(
            array(
                'gal_id' => $gal_id,
                'sub_gal_id' => $sub_gal_id,
                'photo_id' => $photo_id
            )
        );

        return $result;
    }

    public function getGalleryById($token, $gal_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);

        if($token_flag['success'] || $token_flag_admin['success']) {
            $gallerie = DB::table('galleries')->where('id', $gal_id)->first();
            $gallerie = (array)$gallerie;
            $subGalleries = DB::table('sub_gals')->where('gal_id', $gallerie['id'])->get();

            $info['gallerie'] = $gallerie;
            for($a=0; $a<count($subGalleries); $a++) {
                $info['gallerie']['subGalleries'][$a] = (array)$subGalleries[$a];
            }

            for($i=0; $i<count($subGalleries); $i++){
                $photos_ids = DB::table('photo_gallerie')->where('gal_id', $gallerie['id'])->where('sub_gal_id', $subGalleries[$i]->id)->select('photo_id')->get();

                for($j=0; $j<count($photos_ids); $j++) {
                    $ph = DB::table('photos')->where('id', $photos_ids[$j]->photo_id)->first();
                    $info['gallerie']['subGalleries'][$i]['photos'][$j] = (array)$ph;
                }
            }

            if ($info != null && $info != false && !empty($info)) {
                $result['message'] = "Success. There are info.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else if ($info == null) {
                $result['message'] = "No such gallerie.";
                $result['success'] = 0;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    public function getGalleryByIdSingle($gal_id){

            $gallerie = DB::table('galleries')->where('id', $gal_id)->first();
            $gallerie = (array)$gallerie;
            $subGalleries = DB::table('sub_gals')->where('gal_id', $gallerie['id'])->get();

            $info['gallerie'] = $gallerie;
            for($a=0; $a<count($subGalleries); $a++) {
                $info['gallerie']['subGalleries'][$a] = (array)$subGalleries[$a];
            }

            for($i=0; $i<count($subGalleries); $i++){
                $photos_ids = DB::table('photo_gallerie')->where('gal_id', $gallerie['id'])->where('sub_gal_id', $subGalleries[$i]->id)->select('photo_id')->get();

                for($j=0; $j<count($photos_ids); $j++) {
                    $ph = DB::table('photos')->where('id', $photos_ids[$j]->photo_id)->first();
                    $info['gallerie']['subGalleries'][$i]['photos'][$j] = (array)$ph;
                }
            }

        return $info;
    }

    public function getGalleryByName($token, $reg_id,  $gal_name){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $info = array();

        if($token_flag['success'] || $token_flag_admin['success']) {
            $gallery = DB::select("SELECT `id` FROM `galleries` WHERE `name` LIKE '%".$gal_name."%' AND `regionId`=".$reg_id." ORDER BY `pos_num`");
            if(!empty($gallery)) {
                for ($i = 0; $i < count($gallery); $i++) {
                    $info[$i] = $this->getGalleryByIdSingle($gallery[$i]->id);
                }
            }

            if ($info != null && $info != false && !empty($info)) {
                $result['message'] = "Success. There are info.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else if ($info == null) {
                $result['message'] = "No such galleries.";
                $result['success'] = 0;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    public function getGallerys($token, $reg_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $info = array();

        if($token_flag['success'] || $token_flag_admin['success']) {
            $gallery = DB::table('galleries')->where('regionId', $reg_id)->select('id')->orderBy('pos_num')->get();
            for($i=0; $i<count($gallery); $i++){
                $info[$i] = $this->getGalleryByIdSingle($gallery[$i]->id);
            }

            if ($info != null && $info != false && !empty($info)) {
                $result['message'] = "Success. There are info.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else if ($info == null) {
                $result['message'] = "No such galleries.";
                $result['success'] = 0;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    public function deleteGallery($token, $gal_id){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            DB::table('galleries')->where('id', $gal_id)->delete();
            DB::table('sub_gals')->where('gal_id', $gal_id)->delete();
            $photos = DB::table('photo_gallerie')->where('gal_id', $gal_id)->select('photo_id')->get();
            for($i=0; $i<count($photos); $i++){
                DB::table('photos')->where('id', $photos[$i]->photo_id)->delete();
            }
            DB::table('photo_gallerie')->where('gal_id', $gal_id)->delete();

            $result['message'] = "Success.";
            $result['success'] = 1;
            $result['data'] = 1;
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    public function deleteSubGallery($token, $gal_id){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            DB::table('sub_gals')->where('id', $gal_id)->delete();

            $result['message'] = "Success. There are info.";
            $result['success'] = 1;
            $result['data'] = 1;
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = "";
        }

        return $result;
    }

    public function updateImgDescr($img_id, $img_descr){
        $result = DB::table('photos')->where('id', $img_id)->update(
            array(
                'image' => "http://188.226.145.163/kiimia/public/images/".$img_descr
            )
        );

        return (int)$result;
    }

    public function addSubCategorie($token, $sub_gal){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $sub_gals = $sub_gal;

            $sub_gal_id = $this->addSubGallery($sub_gal['gal_id'], $sub_gals);

            $photos = $sub_gals['photos'];
            $photos_ids = array();
            for ($j = 0; $j < count($photos); $j++) {
                $photos_ids[$j] = $this->addPhotoDescr($photos[$j]['description']);
                $res_bind = $this->bindPhotoGallery($sub_gal['gal_id'], $sub_gal_id, $photos_ids[$j]);
            }


            if ($sub_gal_id) {
                $result['message'] = "Success. Gallerie added.";
                $result['success'] = 1;
                $result['sub_gallerie_id'] = $sub_gal_id;
            } else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    //image protocols
    public function createImageProtocol($token, $protocols){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $prt_id = 0;

        if($token_flag_admin['success']) {
            for($i=0; $i<count($protocols); $i++) {
                $prt_id = $this->createProtocol($protocols[$i]);

                if(isset($protocols[$i]['stages']) && !empty($protocols[$i]['stages'])) {
                    $stage = $protocols[$i]['stages'];

                    for ($j = 0; $j < count($stage); $j++)
                        $st_id = $this->createStage($stage[$j], $prt_id);
                }
            }

            if ($prt_id) {
                $result['message'] = "Success. Protocol added.";
                $result['success'] = 1;
                $result['protocol_id'] = $prt_id;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function updateImageProtocol($token, $protocols){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $prt_id = 1;

        if($token_flag_admin['success']) {
            for($i=0; $i<count($protocols); $i++) {
                if(!isset($protocols[$i]['pos_num']))
                    $protocols[$i]['pos_num'] = 0;

                $prt_up = DB::table('image_protocol')->where('id', $protocols[$i]['id'])->update(
                    array(
                        'pos_num' => $protocols[$i]['pos_num'],
                        'regionId' => $protocols[$i]['regionId'],
                        'name' => $protocols[$i]['name'],
                        'description' => $protocols[$i]['description']
                    )
                );
                if(isset($protocols[$i]['stages'])) {
                    $stage = $protocols[$i]['stages'];

                    for ($j = 0; $j < count($stage); $j++) {
                        $st_up = DB::table('stages')->where('id', $stage[$j]['id'])->update(
                            array(
                                'name' => $stage[$j]['name'],
                                'image' => $stage[$j]['image']
                            )
                        );

                        //$photo = DB::table('stages')->where('id', $stage[$j]['id'])->select('photo_id')->first();


                            DB::table('photos')->where('id', $stage[$j]['id'])->update(
                                array(
                                    'description' => $stage[$j]['name'],
                                    'image' => $stage[$j]['image']
                                )
                            );

                    }
                }

            }

            if ($prt_id) {
                $result['message'] = "Success. Protocol updated.";
                $result['success'] = 1;
                $result['data'] = $prt_id;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function createProtocol($protocol){
        if(!isset($protocol['pos_num']))
            $protocol['pos_num'] = 0;

        $result = DB::table('image_protocol')->insertGetId(
            array(
                'pos_num' => $protocol['pos_num'],
                'regionId' => $protocol['regionId'],
                'name' => $protocol['name'],
                'description' => $protocol['description']
            )
        );

        return $result;
    }

    public function createStage($stage, $prt_id){
        $result = DB::table('photos')->insertGetId(
            array(
                'description' => $stage['name']
            )
        );

        DB::table('stages')->insertGetId(
            array(
                'im_pr_id' => $prt_id,
                'name' => $stage['name'],
                'image' => $stage['image'],
                'photo_id' => $result
            )
        );



        return $result;
    }

    public function bindProtocolStage($prt_id, $stg_id){
        $result = DB::table('protocol_stage')->insertGetId(
            array(
                'prt_id' => $prt_id,
                'stg_id' => $stg_id
            )
        );

        return $result;
    }

    public function getImageProtocols($token, $reg_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $info = array();

        if($token_flag_admin['success'] || $token_flag['success']) {
           $protocols = DB::table('image_protocol')->where('regionId', $reg_id)->select('id')->orderBy('pos_num')->get();
           for($i=0; $i<count($protocols); $i++){
               $info[$i] = $this->getProtocolByIdSimple($protocols[$i]->id);
           }

            if (!empty($info)) {
                $result['message'] = "Success. There are info.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function searchImageProtocols($token, $name, $reg_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $info = array();

        if($token_flag_admin['success'] || $token_flag['success']) {
            $protocols = DB::select("SELECT * FROM `image_protocol` WHERE 1 AND `name` LIKE '%".$name."%' AND `regionId`=".$reg_id." ORDER BY `pos_num`");
            if(!empty($protocols)) {
                for ($i = 0; $i < count($protocols); $i++) {
                    $info[$i] = $this->getProtocolByIdSimple($protocols[$i]->id);
                }
            }

            if (!empty($info)) {
                $result['message'] = "Success. There are info.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function getProtocolById($token, $pr_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $info = array();

        if($token_flag_admin['success'] || $token_flag['success']) {
            $protocol = DB::table('image_protocol')->where('id', $pr_id)->first();
            $info['imageProtocols'] = (array)$protocol;
            $stg_ids = DB::table('stages')->leftJoin('photos', 'photos.id', '=', 'stages.photo_id')
                ->where('im_pr_id', $pr_id)
                ->select('stages.id', 'stages.im_pr_id', 'stages.name', 'stages.image', 'stages.photo_id',
                    'photos.id as ph_id', 'photos.description as ph_descr', 'photos.image as ph_img')
                ->get();
            for($i=0; $i<count($stg_ids); $i++){
                $info['imageProtocols']['stages'][$i] = (array)$stg_ids[$i];
            }

            if (!empty($info)) {
                $result['message'] = "Success. There is info.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }

        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function getProtocolByIdSimple($pr_id){
        $info = array();

            $protocol = DB::table('image_protocol')->where('id', $pr_id)->first();
            $info['imageProtocols'] = (array)$protocol;
            $stg_ids = DB::table('stages')->where('im_pr_id', $pr_id)->get();
            for($i=0; $i<count($stg_ids); $i++){
                $info['imageProtocols']['stages'][$i] = (array)$stg_ids[$i];
            }


        return $info;
    }

    public function deleteImageProtocol($token, $pr_id){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            DB::table('image_protocol')->where('id', $pr_id)->delete();
                DB::table('stages')->where('im_pr_id', $pr_id)->delete();

            $result['message'] = "Success.";
            $result['success'] = 1;
            $result['data'] = 1;

        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;

    }

    public function addStageInImageProtocl($token, $pr_id, $stage){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $st_id = DB::table('stages')->insert(
                array(
                    'im_pr_id' => $pr_id,
                    'name' => $stage['name'],
                    'image' => $stage['image']
                )
            );

            if($st_id) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else{
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }

        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function deleteStageInImageProtocl($token, $stg_id){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $st_id = DB::table('stages')->where('id', $stg_id)->delete();

            if($st_id) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else{
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }

        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    //text protocol block
    public function createTextProtocol($token, $protocols){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $prt_id = 0;

        if($token_flag_admin['success']) {
            for($i=0; $i<count($protocols); $i++){
                if(!isset($protocols[$i]['pos_num']))
                    $protocols[$i]['pos_num'] = 0;

                $prt_id = DB::table('text_protocol')->insertGetId(
                    array(
                        'pos_num' => $protocols[$i]['pos_num'],
                        'regionId' => $protocols[$i]['regionId'],
                        'name' => $protocols[$i]['name']
                    )
                );

                $dscr_gr_id = DB::table('description_group')->insertGetId(
                    array(
                        'prt_id' => $prt_id,
                        'name' => $protocols[$i]['descriptionGroup']['name'],
                        'value' => $protocols[$i]['descriptionGroup']['value']
                    )
                );

                $selects_groupe = $protocols[$i]['selectsGroup'];

                $sg_id = DB::table('select_groups')->insertGetId(
                    array(
                        'prt_id' => $prt_id,
                        'name' => $selects_groupe['name']
                    )
                );

                $sg_values = $selects_groupe['values'];
                for($j=0; $j<count($sg_values); $j++){
                    $sg_val_id = DB::table('select_groups_values')->insertGetId(
                        array(
                            'sg_id' => $sg_id,
                            'name' => $sg_values[$j]['name'],
                            'description' => $sg_values[$j]['description']
                        )
                    );

                    if(isset($sg_values[$j]['subSelectGroup']) && !empty($sg_values[$j]['subSelectGroup'])) {
                        $sub_sels = $sg_values[$j]['subSelectGroup'];

                        //for($r=0; $r<count($sub_sels); $r++) {
                        $ssg_id = DB::table('sub_select_group')->insertGetId(
                            array(
                                'sgv_id' => $sg_val_id,
                                'name' => $sub_sels['name']
                            )
                        );

                        if (isset($sub_sels['values']))
                            $ssg_values = $sub_sels['values'];
                        else
                            $ssg_values = "";

                        if (!empty($ssg_values)) {
                            for ($y = 0; $y < count($ssg_values); $y++) {
                                $ssgv_id = DB::table('sub_select_group_values')->insertGetId(
                                    array(
                                        'ssg_id' => $ssg_id,
                                        'name' => $ssg_values[$y]['name']
                                    )
                                );

                                if (isset($ssg_values[$y]['values'])) {
                                    $last_values = $ssg_values[$y]['values'];

                                    for ($e = 0; $e < count($last_values); $e++) {
                                        DB::table('sub_select_group_values_2')->insert(
                                            array(
                                                'ssgv_id' => $ssgv_id,
                                                'name' => $last_values[$e]['name'],
                                                'value' => $last_values[$e]['value']
                                            )
                                        );
                                    }
                                }

                            }
                        }
                    }
                    //}

                }
            }

            if ($prt_id) {
                $result['message'] = "Success. Protocol added.";
                $result['success'] = 1;
                $result['protocol_id'] = $prt_id;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function getTextProtocolById($token, $prt_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);

        if($token_flag_admin['success'] || $token_flag['success']) {
            $protocol = DB::table('text_protocol')->where('id', $prt_id)->first();
            $info = (array)$protocol;

            if($protocol != null) {
                $descr_group = DB::table('description_group')->where('prt_id', $prt_id)->first();
                $info['descriptionGroup'] = (array)$descr_group;

                $selects_group = DB::table('select_groups')->where('prt_id', $prt_id)->first();
                $info['selectsGroup'] = (array)$selects_group;

                $sg_values = DB::table('select_groups_values')->where('sg_id', $selects_group->id)->get();
                for ($i = 0; $i < count($sg_values); $i++) {
                    $info['selectsGroup']['values'][$i] = (array)$sg_values[$i];
                    $ssg = DB::table('sub_select_group')->where('sgv_id', $sg_values[$i]->id)->first();
                    if ($ssg != null) {
                        $info['selectsGroup']['values'][$i]['subSelectGroup'] = (array)$ssg;

                        $ssg_values = DB::table('sub_select_group_values')->where('ssg_id', $ssg->id)->get();
                        if ($ssg_values != null) {
                            for ($j = 0; $j < count($ssg_values); $j++) {
                                $info['selectsGroup']['values'][$i]['subSelectGroup']['values'][$j] = (array)$ssg_values[$j];

                                $last_value = DB::table('sub_select_group_values_2')->where('ssgv_id', $ssg_values[$j]->id)->get();
                                for ($e = 0; $e < count($last_value); $e++) {
                                    $info['selectsGroup']['values'][$i]['subSelectGroup']['values'][$j]['values'][$e] = (array)$last_value[$e];
                                }
                            }
                        }
                    }
                }
            }

            if (!empty($info)) {
                $result['message'] = "Success. There is info.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function updateTextProtocol($token, $protocols){
        $token_flag_admin = $this->isAdminTokenLive($token);


        if($token_flag_admin['success']) {
            if(!isset($protocols['pos_num']))
                $protocols['pos_num'] = 0;

                $prt_id = DB::table('text_protocol')->where('id', $protocols['id'])->update(
                    array(
                        'pos_num' => $protocols['pos_num'],
                        'regionId' => $protocols['regionId'],
                        'name' => $protocols['name']
                    )
                );

                $dscr_gr_id = DB::table('description_group')->where('id', $protocols['descriptionGroup']['id'])->update(
                    array(
                        'prt_id' => $protocols['descriptionGroup']['prt_id'],
                        'name' => $protocols['descriptionGroup']['name'],
                        'value' => $protocols['descriptionGroup']['value']
                    )
                );

                $selects_groupe = $protocols['selectsGroup'];

                $sg_id = DB::table('select_groups')->where('id', $selects_groupe['id'])->update(
                    array(
                        'name' => $selects_groupe['name']
                    )
                );

                if(isset($selects_groupe['values']) && !empty($selects_groupe['values'])) {
                    $sg_values = $selects_groupe['values'];
                    for ($j = 0; $j < count($sg_values); $j++) {
                        $sg_val_id = DB::table('select_groups_values')->where('id', $sg_values[$j]['id'])->update(
                            array(
                                'name' => $sg_values[$j]['name'],
                                'description' => $sg_values[$j]['description']
                            )
                        );

                        if(isset($sg_values[$j]['subSelectGroup']) && !empty($sg_values[$j]['subSelectGroup'])) {
                            $ssg_id = DB::table('sub_select_group')->where('id', $sg_values[$j]['subSelectGroup']['id'])->update(
                                array(
                                    'name' => $sg_values[$j]['subSelectGroup']['name']
                                )
                            );

                            $ssg_values = $sg_values[$j]['subSelectGroup']['values'];

                            for ($y = 0; $y < count($ssg_values); $y++) {
                                $ssgv_id = DB::table('sub_select_group_values')->where('id', $ssg_values[$y]['id'])->update(
                                    array(
                                        'name' => $ssg_values[$y]['name']
                                    )
                                );

                                $last_values = $ssg_values[$y]['values'];

                                for ($e = 0; $e < count($last_values); $e++) {
                                    DB::table('sub_select_group_values_2')->where('id', $last_values[$e]['id'])->update(
                                        array(
                                            'name' => $last_values[$e]['name'],
                                            'value' => $last_values[$e]['value']
                                        )
                                    );
                                }

                            }
                        }

                    }
                }

            if (1) {
                $result['message'] = "Success. Protocol updated.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function getTextProtocolByIdSimple($prt_id){
            $protocol = DB::table('text_protocol')->where('id', $prt_id)->first();
            $info = (array)$protocol;

            $descr_group = DB::table('description_group')->where('prt_id', $prt_id)->first();
            $info['descriptionGroup'] = (array)$descr_group;

            $selects_group = DB::table('select_groups')->where('prt_id', $prt_id)->first();
            $info['selectsGroup'] = (array)$selects_group;

            $sg_values = DB::table('select_groups_values')->where('sg_id', $selects_group->id)->get();
            for($i=0; $i<count($sg_values); $i++) {
                $info['selectsGroup']['values'][$i] = (array)$sg_values[$i];
                $ssg = DB::table('sub_select_group')->where('sgv_id', $sg_values[$i]->id)->first();
                if ($ssg != null){
                    $info['selectsGroup']['values'][$i]['subSelectGroup'] = (array)$ssg;

                $ssg_values = DB::table('sub_select_group_values')->where('ssg_id', $ssg->id)->get();
                for ($j = 0; $j < count($ssg_values); $j++) {
                    $info['selectsGroup']['values'][$i]['subSelectGroup']['values'][$j] = (array)$ssg_values[$j];

                    $last_value = DB::table('sub_select_group_values_2')->where('ssgv_id', $ssg_values[$j]->id)->get();
                    for ($e = 0; $e < count($last_value); $e++) {
                        $info['selectsGroup']['values'][$i]['subSelectGroup']['values'][$j]['values'][$e] = (array)$last_value[$e];
                    }
                }
            }
            }

            return $info;
    }

    public function searchTextProtocol($token, $prt_name, $reg_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $result = array();

        if($token_flag_admin['success'] || $token_flag['success']) {
            $prts = DB::select("SELECT * FROM `text_protocol` WHERE 1 AND `name` LIKE '%".$prt_name."%' AND `regionId`=".$reg_id." ORDER BY `pos_num`");

            if(!empty($prts)){
                for($i=0; $i<count($prts); $i++){
                    $info['textProtocols'][$i] = $this->getTextProtocolByIdSimple($prts[$i]->id);

                    if (!empty($info)) {
                        $result['message'] = "Success. There is info.";
                        $result['success'] = 1;
                        $result['data'] = $info;
                    }
                    else {
                        $result['message'] = "Error.";
                        $result['success'] = 0;
                        $result['data'] = 0;
                    }
                }
            }
            else{
                $result['message'] = "No protocols with such name.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function getTextProtocolByRegion($token, $reg_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $result = array();

        if($token_flag_admin['success'] || $token_flag['success']) {
            $prts = DB::table('text_protocol')->where('regionId', $reg_id)->orderBy('pos_num')->get();
            for($j=0; $j<count($prts); $j++)
                $info['textProtocols'][$j] = (array)$prts[$j];

            if(!empty($prts)){
                for($i=0; $i<count($prts); $i++){
                    $info['textProtocols'][$i] = $this->getTextProtocolByIdSimple($prts[$i]->id);

                    if (!empty($info)) {
                        $result['message'] = "Success. There is info.";
                        $result['success'] = 1;
                        $result['data'] = $info;
                    }
                    else {
                        $result['message'] = "Error.";
                        $result['success'] = 0;
                        $result['data'] = 0;
                    }
                }
            }
            else{
                $result['message'] = "No protocols with such name.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function deleteTextProtocol($token, $prt_id){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $info = DB::table('text_protocol')->where('id', $prt_id)->delete();

            if ($info) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function delete_SelectsGroup_Values_SubSelectGroup_values_values($token, $id){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $info = DB::table('sub_select_group_values_2')->where('id', $id)->delete();

            if ($info) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function add_SelectsGroup_Values_SubSelectGroup_values_values($token, $value){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $info = DB::table('sub_select_group_values_2')->insertGetId(
                array(
                    'ssgv_id' => $value['ssgv_id'],
                    'name' => $value['name'],
                    'value' => $value['value']
                )
            );

            if ($info) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function delete_SelectsGroup_Values_SubSelectGroup_values($token, $id){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $info = DB::table('sub_select_group_values')->where('id', $id)->delete();

            if ($info) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function add_SelectsGroup_Values_SubSelectGroup_values($token, $value){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $info = DB::table('sub_select_group_values_2')->insertGetId(
                array(
                    'ssg_id' => $value['ssg_id'],
                    'name' => $value['name']
                )
            );

            if ($info) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    //categorie block
    public function createCategorie($token, $cat){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $cat_id = 0;

        if($token_flag_admin['success']) {
            for($i=0; $i<count($cat); $i++) {
                if(!isset($cat[$i]['pos_num']))
                    $cat[$i]['pos_num'] = 0;

                $cat_id = DB::table('categories')->insertGetId(
                    array(
                        'pos_num' => $cat[$i]['pos_num'],
                        'regionId' => $cat[$i]['regionId'],
                        'name' => $cat[$i]['name']
                    )
                );

                for($j=0; $j<count($cat[$i]['subCategories']); $j++){
                    DB::table('sub_categorie')->insert(
                        array(
                            'cat_id' => $cat_id,
                            'name' => $cat[$i]['subCategories'][$j]['name'],
                            'description' => $cat[$i]['subCategories'][$j]['description']
                        )
                    );
                }
            }

            if ($cat_id) {
                $result['message'] = "Success. Categorie added.";
                $result['success'] = 1;
                $result['categorie_id'] = $cat_id;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function updateCategorie($token, $cat){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            if(!isset($cat['pos_num']))
                $cat['pos_num'] = 0;

                $cat_update = DB::table('categories')->where('id', $cat['id'])->update(
                    array(
                        'pos_num' => $cat['pos_num'],
                        'regionId' => $cat['regionId'],
                        'name' => $cat['name']
                    )
                );

                for($j=0; $j<count($cat['subCategories']); $j++){
                    if(isset($cat['subCategories'][$j]['id'])) {
                        $sub_cat_update = DB::table('sub_categorie')->where('id', $cat['subCategories'][$j]['id'])->update(
                            array(
                                'name' => $cat['subCategories'][$j]['name'],
                                'description' => $cat['subCategories'][$j]['description']
                            )
                        );
                    }
                }

            if (1) {
                $result['message'] = "Success. Categorie updated.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function getCategorieById($token, $cat_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $info = array();

        if($token_flag_admin['success'] || $token_flag['success']) {
            $cat = DB::table('categories')->where('id', $cat_id)->first();

            if($cat !== null) {
                $info['categories'] = (array)$cat;

                $sub_cats = DB::table('sub_categorie')->where('cat_id', $cat_id)->get();
                for ($i = 0; $i < count($sub_cats); $i++) {
                    $info['categories']['subCategories'][$i] = (array)$sub_cats[$i];
                }
            }

            if (!empty($info)) {
                $result['message'] = "Success. There are categories.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function getCategorieByIdSimple($cat_id){
            $cat = DB::table('categories')->where('id', $cat_id)->first();
            $info['categories'] = (array)$cat;

            $sub_cats = DB::table('sub_categorie')->where('cat_id', $cat_id)->get();
            for($i=0; $i<count($sub_cats); $i++){
                $info['categories']['subCategories'][$i] = (array)$sub_cats[$i];
            }

            return $info;
    }

    public function searchCategorie($token, $reg_id, $name){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $info = array();

        if($token_flag_admin['success'] || $token_flag['success']) {
            $cat_ids = DB::select("SELECT * FROM `categories` WHERE 1 AND `name` LIKE '%".$name."%' AND `regionId`=".$reg_id." ORDER BY `pos_num`");

            for($i=0; $i<count($cat_ids); $i++){
                $info['categories'][$i] = $this->getCategorieByIdSimple($cat_ids[$i]->id);
            }

            if (!empty($info)) {
                $result['message'] = "Success. There are categories.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function deleteCategorie($token, $cat_id){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $info = DB::table('categories')->where('id', $cat_id)->delete();

            if ($info) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function getCategorieByRegionId($token, $reg_id){
        $token_flag_admin = $this->isAdminTokenLive($token);
        $token_flag = $this->isUserTokenLive($token);
        $info = array();

        if($token_flag_admin['success'] || $token_flag['success']) {
            $cat = DB::table('categories')->where('regionId', $reg_id)->orderBy('pos_num')->get();
            for($i=0; $i<count($cat); $i++)
                $info['categories'][$i] = (array)$cat[$i];

            for($j=0; $j<count($cat); $j++) {
                $sub_cats = DB::table('sub_categorie')->where('cat_id', $cat[$j]->id)->get();
                for ($i = 0; $i < count($sub_cats); $i++) {
                    $info['categories'][$j]['subCategories'][$i] = (array)$sub_cats[$i];
                }
            }

            if (!empty($info)) {
                $result['message'] = "Success. There are categorie.";
                $result['success'] = 1;
                $result['data'] = $info;
            }
            else {
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }
        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function addSubCategorieCategorie($token, $values){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $st_id = DB::table('sub_categorie')->insertGetId(
                array(
                    'cat_id' => $values['cat_id'],
                    'name' => $values['name'],
                    'description' => $values['description']
                )
            );

            if($st_id) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else{
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }

        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

    public function deleteSubCategorieCategorie($token, $id){
        $token_flag_admin = $this->isAdminTokenLive($token);

        if($token_flag_admin['success']) {
            $st_id = DB::table('sub_categorie')->where('id', $id)->delete();

            if($st_id) {
                $result['message'] = "Success.";
                $result['success'] = 1;
                $result['data'] = 1;
            }
            else{
                $result['message'] = "Error.";
                $result['success'] = 0;
                $result['data'] = 0;
            }

        }
        else {
            $result['message'] = "Bad token.";
            $result['success'] = 0;
            $result['data'] = 0;
        }

        return $result;
    }

}
